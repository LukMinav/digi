import checkEvolution from "./CheckEvolution.js";
import {showPopup, hidePopup} from "./PopUp.js";
import showMessage from "./Message.js";

export function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function trainStat(stat) {
    showPopup();
    showMessage(`Select a stat to train:`);
    let statIncrease;
    trainingCount++;

    if (stat === "power") {
        statIncrease = getRandomNumber(50, 175);
        power += statIncrease;
        showMessage(`Your ${stat.toUpperCase()} has increased by ${statIncrease}`);
        checkEvolution();
        updateDigimonInfo();
    } else if (stat === "speed") {
        statIncrease = getRandomNumber(50, 175);
        speed += statIncrease;
        showMessage(`Your ${stat.toUpperCase()} has increased by ${statIncrease}`);
        checkEvolution();
        updateDigimonInfo();
    } else if (stat === "wisdom") {
        statIncrease = getRandomNumber(50, 175);
        wisdom += statIncrease;
        showMessage(`Your ${stat.toUpperCase()} has increased by ${statIncrease}`);
        checkEvolution();
        updateDigimonInfo();
    } else if (stat === "hp") {
        statIncrease = getRandomNumber(425, 525);
        hp += statIncrease;
        showMessage(`Your ${stat.toUpperCase()} has increased by ${statIncrease}`);
        checkEvolution();
        updateDigimonInfo();
    } else if (stat === "mp") {
        statIncrease = getRandomNumber(425, 525);
        mp += statIncrease;
        showMessage(`Your ${stat.toUpperCase()} has increased by ${statIncrease}`);
        checkEvolution();
        updateDigimonInfo();
    }
    hidePopup();
}