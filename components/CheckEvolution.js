let evolutionDigimon1 = 'Exveemon';
let evolutionDigimon2 = 'Veedramon';
let evolutionDigimon3 = 'Greymon';
let evolutionDigimon4 = 'Numemon';
let trainingCount = 0;

export default function checkEvolution() {
    if (power >= 550 && wisdom >= 500 && speed >=500) {
        showMessage(`${actualDigimon} Evolves to ${evolutionDigimon1}!`);
        actualDigimon = evolutionDigimon1;
        updateDigimonImage();
    } else if (speed >= 400 && hp>=1500 && power>=400) {
        showMessage(`${actualDigimon} Evolves to ${evolutionDigimon2}!`);
        actualDigimon = evolutionDigimon2;
        updateDigimonImage();
    } else if (hp > 2200 && power > 500) {
        showMessage(`${actualDigimon} Evolves to ${evolutionDigimon3}!`);
        actualDigimon = evolutionDigimon3;
        updateDigimonImage();
    } else if (trainingCount>=10) {
        showMessage(`You didn't get good results! ${actualDigimon} Evolves to ${evolutionDigimon4}...`);
        actualDigimon = evolutionDigimon4; 
        updateDigimonImage();
    }
}