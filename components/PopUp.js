export function showPopup() {
    const popup = document.getElementById("popup");
    popup.style.display = "block";
}

export function hidePopup() {
    const popup = document.getElementById("popup");
    popup.style.display = "none";
}