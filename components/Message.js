let messageTimer; // Variable para guardar el temporizador

export function showMessage(message) {
    const messageArea = document.getElementById("messageArea");
    messageArea.textContent = message;

    // Borra el mensaje después de 8 segundos
    clearTimeout(messageTimer);
    messageTimer = setTimeout(clearMessage, 8000);
}

export function clearMessage() {
    const messagesContainer = document.getElementById("messages");
    messagesContainer.innerHTML = "";
}

