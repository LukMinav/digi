import {updateDigimonImage, updateDigimonInfo } from "./UpdateDigimon.js";
import { showMessage, clearMessage } from "./Message.js";

export default function resetGame() {
  digimonActual.power = 300;
  digimonActual.speed = 300;
  digimonActual.wisdom = 300;
  digimonActual.hp = 1000;
  digimonActual.mp = 1000;
  digimonActual.name = 'Veemon';
  trainingCount = 0;
  updateDigimonImage(digimonActual.name);
  digimonImage.style.width = '250px';
  digimonImage.style.height = '250px';
  updateDigimonInfo();
  showMessage("Game reset successfully!");
}

