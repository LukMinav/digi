let digimonActual = {
    name: 'Veemon',
    power: 300,
    speed: 300,
    wisdom: 300,
    hp: 1000,
    mp: 1000
  };
  let trainingCount = 0;

export function updateDigimonImage() {
    const digimonName = actualDigimon.charAt(0).toLowerCase() + actualDigimon.slice(1);
    const digimonImage = document.getElementById("digimonImage");
    digimonImage.src = `images/${digimonName}.gif`;
    digimonImage.alt = actualDigimon;
    digimonImage.style.width = '350px';
    digimonImage.style.height = '350px';
}


export function updateDigimonInfo() {
    document.getElementById("actualDigimon").textContent = digimonActual.name;
    document.getElementById("power").textContent = digimonActual.power;
    document.getElementById("speed").textContent = digimonActual.speed;
    document.getElementById("wisdom").textContent = digimonActual.wisdom;
    document.getElementById("hp").textContent = digimonActual.hp;
    document.getElementById("mp").textContent = digimonActual.mp;
  }
  