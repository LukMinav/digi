let veemon = {
    power: 300,
    speed: 300,
    wisdom: 300,
    hp: 1000,
    mp: 1000,
    name: 'Veemon',
    alt: 'Veemon',
    image: 'images/veemon.gif',
    width: '225px',
    height: '225px',
};

let currentDigimon = veemon
let rivalDigimon;
  
  let evolutionDigimon1 = {
    power: null,
    speed: null,
    wisdom: null,
    hp: null,
    mp: null,
    name: 'Exveemon', 
    alt: 'Exveemon',
    image: 'images/exveemon.gif',
    width: '350px',
    height: '350px',
  };
  
  let evolutionDigimon2 = {
    power: null,
    speed: null,
    wisdom: null,
    hp: null,
    mp: null,
    name: 'Veedramon',
    alt: 'Veedramon',
    image: 'images/veedramon.gif',
    width: '375px',
    height: '375px',
  };
  
  let evolutionDigimon3 = {
    power: null,
    speed: null,
    wisdom: null,
    hp: null,
    mp: null,
    name:'Greymon',
    alt: 'Greymon',
    image: 'images/greymon.gif',
    width: '375px',
    height: '375px',
  };
  
  let evolutionDigimon4 = {
    power: null,
    speed: null,
    wisdom: null,
    hp: null,
    mp: null,
    name: 'Numemon',
    image: 'images/numemon.gif',
    width: '275px',
    height: '275px',
  };
  
  let goblimon = {
    power: 5000,
    speed: 3000,
    wisdom: 200,
    hp: 1200,
    mp: 1000,
    name: 'Goblimon',
    alt: 'Goblimon',
    image: 'images/goblimon.gif',
};
  
  let devimon = {
    power: 800,
    speed: 10000,
    wisdom: 1000,
    hp: 1600,
    mp: 2000,
    name: 'Devimon',
    alt: 'Devimon',
    image: 'images/devimon.gif',
    width: '375px',
    height: '375px',
  };
  
  let trainingCount = 0;
  let maxTrainingCount = 10;
  let didEvolve = false;
  
  /*function startFight(digimon) {
    rivalDigimon === goblimon ? document.getElementById("goblimonContainer") : document.getElementById("devimonContainer");
  
    rivalImage.style.visibility = "visible";
    showMessage("Digimons are fighting!");
  
    setTimeout(() => {
      rivalImage.style.visibility = "hidden";
      fight(digimon);
    }, 5000);
  }

  
  function fightResults(currentDigimon, rivalDigimon) {
    let currentDigimonFightPoints = currentDigimon.power + currentDigimon.speed + currentDigimon.wisdom + currentDigimon.hp / 10 + currentDigimon.mp / 10;
  
    let rivalDigimonFightPoints = rivalDigimon.power + rivalDigimon.speed + rivalDigimon.wisdom + rivalDigimon.hp / 10 + rivalDigimon.mp / 10;
  
    if (currentDigimonFightPoints > rivalDigimonFightPoints) {
      showMessage(`${currentDigimon.name} wins the fight!`);
    } else if (currentDigimonFightPoints < rivalDigimonFightPoints) {
      showMessage(`${rivalDigimon.name} wins the fight!`);
    } else {
      showMessage(`It's a tie!`);
    }
  }

    if (currentDigimon.name === digimon.name) {
        digimonImage.style.visibility = "hidden";
        showMessage("Your Digimon has lost the battle. Game Over!");
        setTimeout(() => {
            resetGame();
            digimonImage.style.visibility = "visible";
            fightButton1.style.display = "block";
            fightButton2.style.display = "block";
        }, 5000);
    } else {
        fightButton1.style.display = "none";
        fightButton2.style.display = "none";
        showMessage(`Your Digimon won the battle against ${digimon.name}!`);
        const rand = getRandomNumber(1, 3);
        switch (rand) {
            case 1:
                currentDigimon.power += 150;
                currentDigimon.speed += 50;
                showMessage("Your Digimon has won and got stronger!. Power increased by 150 and Speed by 50.");
                break;
            case 2:
                currentDigimon.speed += 50;
                currentDigimon.wisdom += 150;
                showMessage("Your Digimon has won and got wiser! Wisdom increased by 150 and Speed by 50.");
                break;
            case 3:
                currentDigimon.hp += 800;
                currentDigimon.mp += 200;
                showMessage("Your Digimon has won and got healthier! HP increased by 800 and MP by 200.");
                break;
        }
        updateDigimonInfo();
    }
}*/

  function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  
  let messageTimer; 
  
  function showMessage(message) {
    const messageArea = document.getElementById("messageArea");
    messageArea.textContent = message;
    clearTimeout(messageTimer);
    messageTimer = setTimeout(clearMessage, 8000);
  }
  
  function clearMessage() {
    const messageArea = document.getElementById("messageArea");
    messageArea.textContent = "";
  }
  
  function showPopup() {
    const popup = document.getElementById("popup");
    if (popup.style.display === "block") {
        popup.style.display = "none";
    } else {
        popup.style.display = "block";
    }
}
  function hidePopup() {
    const popup = document.getElementById("popup");
    popup.style.display = "none";
  }

  function disableTrainingButtons() {
    const trainingButtons = document.querySelectorAll(".training-button");
    trainingButtons.forEach(button => {
        button.disabled = true;
    });
}

function enableTrainingButtons() {
    const trainingButtons = document.querySelectorAll(".training-button");
    trainingButtons.forEach(button => {
        button.disabled = false;
    });
}
  
function trainStat(stat) {
  if (trainingCount >= maxTrainingCount) {
      showMessage("You can't train no more!");
      return;
  }
  showMessage(`Select a stat to train:`);
  let statIncrease;
  trainingCount++;

  const digimonImage = document.getElementById("digimonImage");
  const originalImage = digimonImage.src;
  digimonImage.src = 'images/training.gif';

  disableTrainingButtons();
  setTimeout(enableTrainingButtons, 500);

  switch (stat) {
      case "power":
          statIncrease = getRandomNumber(100, 175);
          currentDigimon.power += statIncrease;
          showMessage(`Your ${stat.toUpperCase()} has increased by ${statIncrease}`);
          updateDigimonInfo();
          break;
      case "speed":
          statIncrease = getRandomNumber(100, 175);
          currentDigimon.speed += statIncrease;
          showMessage(`Your ${stat.toUpperCase()} has increased by ${statIncrease}`);
          updateDigimonInfo();
          break;
      case "wisdom":
          statIncrease = getRandomNumber(100, 175);
          currentDigimon.wisdom += statIncrease;
          showMessage(`Your ${stat.toUpperCase()} has increased by ${statIncrease}`);
          updateDigimonInfo();
          break;
      case "hp":
          statIncrease = getRandomNumber(300, 375);
          currentDigimon.hp += statIncrease;
          showMessage(`Your ${stat.toUpperCase()} has increased by ${statIncrease}`);
          updateDigimonInfo();
          break;
      case "mp":
          statIncrease = getRandomNumber(300, 375);
          currentDigimon.mp += statIncrease;
          showMessage(`Your ${stat.toUpperCase()} has increased by ${statIncrease}`);
          updateDigimonInfo();
          break;
  }

  setTimeout(() => {
      digimonImage.src = originalImage;
      checkEvolution();
  }, 500);
}
  
function checkEvolution() {
  if (didEvolve) {
    return;
  }
  function evolveCallback(evolutionName, evolutionData, statChanges) {
      showMessage(`Your digimon is evolving!`);
      const digimonImage = document.getElementById("digimonImage");
      digimonImage.src = 'images/digivolve.gif';
      hidePopup();
      disableTrainingButtons();

      setTimeout(function () {
          showMessage(`${currentDigimon.name} Evolves to ${evolutionName}!`);
          currentDigimon.name = evolutionData.name;
          currentDigimon.image = evolutionData.image;
          currentDigimon.alt = evolutionData.alt;
          currentDigimon.width = evolutionData.width;
          currentDigimon.height = evolutionData.height;

          if (statChanges) {
              currentDigimon.speed += statChanges.speed || 0;
              currentDigimon.power += statChanges.power || 0;
              currentDigimon.wisdom += statChanges.wisdom || 0;
              currentDigimon.hp += statChanges.hp || 0;
              currentDigimon.mp += statChanges.mp || 0;
          }

          updateDigimonImage();
          updateDigimonInfo();
          enableTrainingButtons();
      }, 3000);
  }

  switch (true) {
      case currentDigimon.power >= 550 && currentDigimon.wisdom >= 500 && currentDigimon.speed >= 500:
          disableTrainingButtons();
          if (!didEvolve) {
              evolveCallback(evolutionDigimon1.name, evolutionDigimon1, {
                  power: 500,
                  speed: 500,
                  wisdom: 200,
                  hp: 1000,
                  mp: 1000,
              });
              didEvolve = true;
          }
          break;

      case currentDigimon.speed >= 400 && currentDigimon.hp >= 1500 && currentDigimon.power >= 400:
          disableTrainingButtons();
          if (!didEvolve) {
              evolveCallback(evolutionDigimon2.name, evolutionDigimon2, {
                  power: 400,
                  speed: 100,
                  wisdom: 100,
                  hp: 1500,
                  mp: 500,
              });
              didEvolve = true;
          }
          break;

      case currentDigimon.hp > 2000 && currentDigimon.power > 500:
          disableTrainingButtons();
          if (!didEvolve) {
              evolveCallback(evolutionDigimon3.name, evolutionDigimon3, {
                power: 300,
                speed: 100,
                wisdom: 100,
                hp: 2000,
                mp: 100,
              });
              didEvolve = true;
          }
          break;

      case trainingCount >= 10:
          disableTrainingButtons();
          if (!didEvolve) {
              evolveCallback(evolutionDigimon4.name, evolutionDigimon4, {
                power: 100,  
                speed: 100,
                wisdom: -250,
                hp: 200,
                mp: 500,
              });
              didEvolve = true;
          }
          break;
  }
}

function resetGame() {
  showMessage("Game reset successfully!");
  didEvolve = false;
  currentDigimon = {
    power: 300,
    speed: 300,
    wisdom: 300,
    hp: 1000,
    mp: 1000,
    name: 'Veemon',
    alt: 'Veemon',
    image: 'images/veemon.gif',
    width: '225px', 
    height: '225px',
};
  trainingCount = 0;
  updateDigimonInfo(); 
  updateDigimonImage(); 
  
}

  
    function updateDigimonInfo() {
        document.getElementById("currentDigimon.name").textContent = currentDigimon.name;
        document.getElementById("currentDigimon.power").textContent = currentDigimon.power;
        document.getElementById("currentDigimon.speed").textContent = currentDigimon.speed;
        document.getElementById("currentDigimon.wisdom").textContent = currentDigimon.wisdom;
        document.getElementById("currentDigimon.hp").textContent = currentDigimon.hp;
        document.getElementById("currentDigimon.mp").textContent = currentDigimon.mp;
    }

    function updateDigimonImage() {
        const digimonImage = document.getElementById("digimonImage");
        digimonImage.src = currentDigimon.image;
        digimonImage.alt = currentDigimon.alt;
        digimonImage.style.width = currentDigimon.width;
        digimonImage.style.height = currentDigimon.height;
      }